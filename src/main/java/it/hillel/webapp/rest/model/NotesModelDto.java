package it.hillel.webapp.rest.model;

import java.util.Map;

public class NotesModelDto {
    private Map<Integer, String> notes;

    public NotesModelDto(Map<Integer, String> notes) {
        this.notes = notes;
    }

    public Map<Integer, String> getNotes() {
        return notes;
    }

    public void setNotes(Map<Integer, String> notes) {
        this.notes = notes;
    }
}
