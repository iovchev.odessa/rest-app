package it.hillel.webapp.rest.model;

public class NoteModelDto {
    int id;
    String note;

    public NoteModelDto(int id, String note) {
        this.id = id;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
