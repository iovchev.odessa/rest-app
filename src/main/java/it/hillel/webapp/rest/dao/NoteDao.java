package it.hillel.webapp.rest.dao;

import it.hillel.webapp.rest.exception.ApplicationException;
import it.hillel.webapp.rest.component.SecurityContext;
import it.hillel.webapp.rest.entity.NoteEntity;
import it.hillel.webapp.util.ConnectionManager;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Stream;

public class NoteDao {

    public NoteEntity createNewNote(String note) {
        int id = 0;
        String sgl = """
                INSERT INTO user_storage.notes(user_id, note)
                VALUES (?,?);
                """;
        try (Connection connection = ConnectionManager.open();
             PreparedStatement preparedStatement = connection.prepareStatement(sgl, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setInt(1, SecurityContext.get().getId());
            preparedStatement.setString(2, note);
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            while (resultSet.next()) {
                id = (int) resultSet.getObject(1);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return new NoteEntity(id, note);

    }

    public NoteEntity deleteNote(int id) {
        String note = getNote(id);
        String sgl = """
                DELETE FROM user_storage.notes
                WHERE  id = ? AND user_id = ?;
                """;
        try (Connection connection = ConnectionManager.open();
             PreparedStatement preparedStatement = connection.prepareStatement(sgl)) {
            preparedStatement.setInt(1, id);
            preparedStatement.setInt(2, SecurityContext.get().getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return new NoteEntity(id, note);
    }

    private String getNote(int id) {
        String result = null;
        String sgl = """
                SELECT *
                FROM user_storage.notes
                WHERE  id = ? AND user_id = ?;
                """;
        try (Connection connection = ConnectionManager.open();
             PreparedStatement preparedStatement = connection.prepareStatement(sgl, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setInt(1, id);
            preparedStatement.setInt(2, SecurityContext.get().getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                result = resultSet.getString("note");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        if (result != null) {
            return result;
        } else {
            throw new ApplicationException("Note doesn't exist.");
        }
    }

    public NoteEntity updateNote(int id, String note) {
        getNote(id);
        String sgl = """
                UPDATE user_storage.notes
                SET note = ?
                WHERE  id = ? AND user_id = ?;
                """;
        try (Connection connection = ConnectionManager.open();
             PreparedStatement preparedStatement = connection.prepareStatement(sgl)) {
            preparedStatement.setString(1, note);
            preparedStatement.setInt(2, id);
            preparedStatement.setInt(3, SecurityContext.get().getId());
            preparedStatement.executeUpdate();


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return new NoteEntity(id, note);
    }

    public NoteEntity getAllNote() {
        Map<Integer, String> mapNotes = new TreeMap<>();
        String sgl = """
                SELECT *
                FROM user_storage.notes
                WHERE  user_id = ?;
                """;
        try (Connection connection = ConnectionManager.open();
             PreparedStatement preparedStatement = connection.prepareStatement(sgl, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setInt(1, SecurityContext.get().getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                mapNotes.put(resultSet.getInt("id"), resultSet.getString("note"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        NoteEntity noteEntity = new NoteEntity();
        noteEntity.setNotes(mapNotes);
        return noteEntity;
    }

    public NoteEntity getNoteById(int id) {
        String note = null;
        String sgl = """
                SELECT *
                FROM user_storage.notes
                WHERE  id = ? AND user_id = ?;
                """;
        try (Connection connection = ConnectionManager.open();
             PreparedStatement preparedStatement = connection.prepareStatement(sgl)) {
            preparedStatement.setInt(1, id);
            preparedStatement.setInt(2, SecurityContext.get().getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                note = resultSet.getString("note");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        if (note != null) {
             return  new NoteEntity(id, note);
        } else {
            throw new ApplicationException("Note doesn't exist.");
        }
    }
}
