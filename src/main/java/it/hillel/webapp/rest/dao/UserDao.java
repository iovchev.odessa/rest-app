package it.hillel.webapp.rest.dao;

import it.hillel.webapp.rest.exception.ApplicationException;
import it.hillel.webapp.rest.entity.UserEntity;
import it.hillel.webapp.util.ConnectionManager;

import java.sql.*;

public class UserDao {

    public UserDao() {
    }

    public UserEntity createNewUser(String login, String lastName, String firstName, String password) {
        if (userExist(login)) {
            throw new ApplicationException("User with the same login exist. Enter other login.");
        }
        int id = 0;
        String sgl = """
                INSERT INTO user_storage.user(user_login,first_name, last_name, user_password)
                VALUES (?,?,?,?);
                """;
        try (Connection connection = ConnectionManager.open();
             PreparedStatement preparedStatement = connection.prepareStatement(sgl, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, firstName);
            preparedStatement.setString(3, lastName);
            preparedStatement.setString(4, password);
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            while (resultSet.next()) {
                id = (int) resultSet.getObject(1);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return new UserEntity(id, login, lastName, firstName, password);
    }

    private boolean userExist(String login) {
        String sgl = """ 
                SELECT *
                FROM user_storage.user
                WHERE user_login = ?;
                """;
        try (Connection connection = ConnectionManager.open();
             PreparedStatement preparedStatement = connection.prepareStatement(sgl)) {
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return true;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return false;
    }


    public UserEntity getUserByLogin(String login) {
        int id = 0;
        String firstName = "";
        String lastName = "";
        String password = "";
        String sgl = """ 
                SELECT *
                FROM user_storage.user
                WHERE user_login = ?;
                """;
        try (Connection connection = ConnectionManager.open();
             PreparedStatement preparedStatement = connection.prepareStatement(sgl)) {
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                id = resultSet.getInt("id");
                firstName = resultSet.getString("first_name");
                lastName = resultSet.getString("last_name");
                password = resultSet.getString("user_password");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        if (id != 0) {
            return new UserEntity(id, login, lastName, firstName, password);
        } else {
            throw new ApplicationException("Invalid login entered.");
        }
    }

    public UserEntity getUserById(int id) {
        String login = "";
        String firstName = "";
        String lastName = "";
        String password = "";
        String sgl = """ 
                SELECT *
                FROM user_storage.user
                WHERE id = ?;
                """;
        try (Connection connection = ConnectionManager.open();
             PreparedStatement preparedStatement = connection.prepareStatement(sgl)) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                id = resultSet.getInt("id");
                login = resultSet.getString("user_login");
                firstName = resultSet.getString("first_name");
                lastName = resultSet.getString("last_name");
                password = resultSet.getString("user_password");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        if (login != "") {
            return new UserEntity(id, login, lastName, firstName, password);
        } else {
            throw new ApplicationException("User didn't be find.");
        }
    }
}
