package it.hillel.webapp.rest.web;

import it.hillel.webapp.rest.dto.NoteDto;
import it.hillel.webapp.rest.entity.NoteEntity;
import it.hillel.webapp.rest.exception.ApplicationException;
import it.hillel.webapp.rest.model.NoteModelDto;
import it.hillel.webapp.rest.model.NotesModelDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/api/v1/note/*", loadOnStartup = 1)
public class NoteRestServlet extends RestServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        NoteDto dto = this.getObjectMapper().readValue(req.getInputStream(), NoteDto.class);

        try {

            NoteEntity note = this.getNoteService().insert(dto.getNote());
            String msg = "Your note has been added.";
            responseNote(resp, note, msg);

        } catch (ApplicationException e) {
            exceptionResponse(resp, e);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        NoteDto dto = this.getObjectMapper().readValue(req.getInputStream(), NoteDto.class);

        try {

            NoteEntity note = this.getNoteService().update(Integer.parseInt(dto.getId()), dto.getNote());
            String msg = "Your note has been updated.";
            responseNote(resp, note, msg);

        } catch (ApplicationException e) {
            exceptionResponse(resp, e);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        NoteDto dto = this.getObjectMapper().readValue(req.getInputStream(), NoteDto.class);
        try {
            NoteEntity note = this.getNoteService().delete(Integer.parseInt(dto.getId()));
            String msg = "Your note has been deleted.";
            responseNote(resp, note, msg);
        } catch (ApplicationException e) {
            exceptionResponse(resp, e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            String path = getServletContext().getContextPath() + "/api/v1/note";
            String pathSlash = getServletContext().getContextPath() + "/api/v1/note/";
            if (path.equals(req.getRequestURI()) || pathSlash.equals(req.getRequestURI())) {
                NoteEntity note = this.getNoteService().getAllNotes();
                resp.getWriter().write(this.getObjectMapper().writeValueAsString(new NotesModelDto(note.getNotes())));
            } else {
                String[] array = req.getRequestURI().split("/");
                int idNote = Integer.parseInt(array[array.length - 1]);
                NoteEntity note = this.getNoteService().getNote(idNote);
                resp.getWriter().write(this.getObjectMapper().writeValueAsString(new NoteModelDto(note.getId(), note.getNote())));
            }

            resp.setStatus(200);
            resp.setContentType("application/json");
            resp.getWriter().flush();

        } catch (ApplicationException e) {
            exceptionResponse(resp, e);
        }
    }
}
