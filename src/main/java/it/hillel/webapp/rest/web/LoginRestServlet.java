package it.hillel.webapp.rest.web;


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import it.hillel.webapp.rest.entity.UserEntity;
import it.hillel.webapp.rest.exception.ApplicationException;
import it.hillel.webapp.rest.model.TokenDto;
import it.hillel.webapp.rest.dto.LoginDto;
import it.hillel.webapp.rest.validator.LoginDtoValidator;
import it.hillel.webapp.rest.service.LoginService;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.UUID;

@WebServlet(urlPatterns = "/api/v1/auth/login", loadOnStartup = 1)
public class LoginRestServlet extends RestServlet {
    private final LoginService loginService = new LoginService();

    private final LoginDtoValidator loginDtoValidator = new LoginDtoValidator();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        LoginDto dto = this.getObjectMapper().readValue(req.getInputStream(), LoginDto.class);

        try {

            this.loginDtoValidator.validate(dto);
            Date now = new Date();
            UserEntity user = this.loginService.login(dto.getLogin(), dto.getPassword());

            String token = Jwts.builder().
                    setSubject(String.valueOf(user.getId())).
                    setIssuedAt(now).
                    setExpiration(new Date(now.getTime() + 30 * 60 * 1000)).
                    setId(UUID.randomUUID().toString()).
                    signWith(Keys.hmacShaKeyFor("Yn2kjibddFAWtnPJ2AFlL8WXmohJMCvigQggaEypa5E=".getBytes(StandardCharsets.UTF_8)), SignatureAlgorithm.HS256).
                    compact();

            resp.setStatus(200);
            resp.setContentType("application/json");
            resp.getWriter().write(this.getObjectMapper().writeValueAsString(new TokenDto(token)));
            resp.getWriter().flush();

        } catch (ApplicationException e) {
            exceptionResponse(resp,e);
        }
    }
}
