package it.hillel.webapp.rest.web;

import it.hillel.webapp.rest.dto.ResponseDto;
import it.hillel.webapp.rest.exception.ApplicationException;
import it.hillel.webapp.rest.dto.RegisterDto;
import it.hillel.webapp.rest.entity.UserEntity;
import it.hillel.webapp.rest.model.UserDto;
import it.hillel.webapp.rest.validator.RegisterDtoValidator;
import it.hillel.webapp.rest.service.LoginService;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/api/v1/auth/register", loadOnStartup = 1)
public class RegisterRestServlet extends RestServlet {
    private final LoginService loginService = new LoginService();
    private final RegisterDtoValidator registerDtoValidator = new RegisterDtoValidator();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        RegisterDto dto = this.getObjectMapper().readValue(req.getInputStream(), RegisterDto.class);

        try {

            this.registerDtoValidator.validate(dto);

            UserEntity user = this.loginService.register(dto.getLogin(), dto.getLastName(), dto.getFirstName(), dto.getPassword());

            resp.setStatus(200);
            resp.setContentType("application/json");
            resp.getWriter().write(this.getObjectMapper().writeValueAsString(new UserDto(user.getId(), user.getLogin(), user.getLastName(), user.getFirstName())));
            resp.getWriter().write(this.getObjectMapper().writeValueAsString(new ResponseDto("You registered successfully.")));
            resp.getWriter().flush();

        } catch (ApplicationException e) {
            exceptionResponse(resp,e);
        }
    }
}
