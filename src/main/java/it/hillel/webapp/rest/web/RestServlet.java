package it.hillel.webapp.rest.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.hillel.webapp.rest.exception.ApplicationException;
import it.hillel.webapp.rest.dto.ResponseDto;
import it.hillel.webapp.rest.entity.NoteEntity;
import it.hillel.webapp.rest.model.NoteModelDto;
import it.hillel.webapp.rest.service.NoteService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

abstract class RestServlet extends HttpServlet {

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final NoteService noteService = new NoteService();

    public NoteService getNoteService() {
        return noteService;
    }

    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    void exceptionResponse(HttpServletResponse resp, ApplicationException e) throws IOException {
        resp.setStatus(400);
        resp.setContentType("application/json");
        resp.getWriter().write(this.objectMapper.writeValueAsString(new ResponseDto(e.getMessage())));
        resp.getWriter().flush();
    }

    void responseNote(HttpServletResponse resp, NoteEntity note, String msg) throws IOException {
        resp.setStatus(200);
        resp.setContentType("application/json");
        resp.getWriter().write(this.getObjectMapper().writeValueAsString(new NoteModelDto(note.getId(), note.getNote())));
        resp.getWriter().write(this.getObjectMapper().writeValueAsString(new ResponseDto(msg)));
        resp.getWriter().flush();
    }
}
