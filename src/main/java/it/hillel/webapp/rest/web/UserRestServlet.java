package it.hillel.webapp.rest.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.hillel.webapp.rest.exception.ApplicationException;
import it.hillel.webapp.rest.component.SecurityContext;
import it.hillel.webapp.rest.entity.UserEntity;
import it.hillel.webapp.rest.model.UserDto;

import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "UserRestServlet", value = "/api/v1/user")
public class UserRestServlet extends RestServlet {
    private final ObjectMapper objectMapper = new ObjectMapper();


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
        UserEntity user = SecurityContext.get();

        response.setStatus(200);
        response.setContentType("application/json");
        response.getWriter().write(this.objectMapper.writeValueAsString(new UserDto(user.getId(), user.getLogin(), user.getLastName(), user.getFirstName())));
        response.getWriter().flush();
        } catch (ApplicationException e) {
            exceptionResponse(response,e);
        }
    }


}
