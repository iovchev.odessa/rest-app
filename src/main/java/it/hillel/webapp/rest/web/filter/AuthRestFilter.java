package it.hillel.webapp.rest.web.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import it.hillel.webapp.rest.component.SecurityContext;
import it.hillel.webapp.rest.entity.UserEntity;
import it.hillel.webapp.rest.service.UserService;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@WebFilter(filterName = "AuthRestFilter", urlPatterns = "/*")
public class AuthRestFilter implements Filter {

    private final List<String> PROTECTED_PATH = new ArrayList<>();
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final UserService userService = new UserService();

    private JwtParser jwtParser;

    public void init(FilterConfig config) throws ServletException {

        this.jwtParser = Jwts.parserBuilder()
                .setSigningKey(Keys.hmacShaKeyFor("Yn2kjibddFAWtnPJ2AFlL8WXmohJMCvigQggaEypa5E=".getBytes(StandardCharsets.UTF_8)))
                .build();

        PROTECTED_PATH.add("/api/v1/user");
        PROTECTED_PATH.add("/api/v1/note");
    }

    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        String requestUri = req.getRequestURI().replace(req.getContextPath(), "");

        if (PROTECTED_PATH.stream().anyMatch(url -> requestUri.startsWith((url)))) {
            String authHeader = req.getHeader("Authorization");

            if (authHeader == null || authHeader.isEmpty()) {
                resp.setStatus(401);
                resp.setContentType("application/json");
                resp.getWriter().write(this.objectMapper.writeValueAsString("Where is token?"));
                resp.getWriter().flush();

                return;
            }

            String jwtToken = authHeader.replace("Bearer ", "");

            Claims claims = this.jwtParser.parseClaimsJws(jwtToken).getBody();

            UserEntity user = this.userService.findById(Integer.parseInt(claims.getSubject()));

            SecurityContext.set(user);
            chain.doFilter(request, response);
        } else {
            chain.doFilter(request, response);
        }
    }
}
