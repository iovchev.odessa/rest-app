package it.hillel.webapp.rest.service;

import it.hillel.webapp.rest.entity.UserEntity;
import it.hillel.webapp.rest.exception.ApplicationException;

public class LoginService {
    public UserEntity login(String login, String password) {
        UserEntity userEntity = UserEntity.getUserEntityByLogin(login);

        if (!password.equals(userEntity.getPassword())) {
            throw new ApplicationException("Password incorrect");
        }

        return userEntity;
    }

    public UserEntity register(String login, String lastName, String firstName, String password) {
        UserEntity userEntity = new UserEntity();
        return  userEntity.createUserEntity(login, lastName, firstName, password);

    }
}

