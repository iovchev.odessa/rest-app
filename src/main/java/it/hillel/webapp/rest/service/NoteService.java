package it.hillel.webapp.rest.service;

import it.hillel.webapp.rest.exception.ApplicationException;
import it.hillel.webapp.rest.entity.NoteEntity;

import java.util.Map;

public class NoteService {
    Map notes;

    public NoteService() {
    }

    public Map getNotes() {
        return notes;
    }

    public void setNotes(Map notes) {
        this.notes = notes;
    }


    public NoteEntity insert(String note) {
        if(note.isEmpty()){
            throw new ApplicationException("You don't add empty note.");
        }
        NoteEntity noteEntity = new NoteEntity();
        return  noteEntity.createNoteEntity(note);
    }

    public NoteEntity delete(int id) {
        NoteEntity noteEntity = new NoteEntity();
        return  noteEntity.deleteNoteEntity(id);
    }

    public NoteEntity update(int id, String note) {
        NoteEntity noteEntity = new NoteEntity();
        return  noteEntity.updateNoteEntity(id, note);
    }

    public NoteEntity getAllNotes() {
        NoteEntity noteEntity = new NoteEntity();
        return  noteEntity.getAllNoteEntity();
    }


    public NoteEntity getNote(int id) {
        NoteEntity noteEntity = new NoteEntity();
        return  noteEntity.getNoteEntity(id);
    }
}
