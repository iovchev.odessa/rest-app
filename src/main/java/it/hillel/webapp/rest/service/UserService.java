package it.hillel.webapp.rest.service;

import it.hillel.webapp.rest.entity.UserEntity;

public class UserService {
    public UserEntity findById(int id){
        return UserEntity.getUserEntityById(id);
    }
}
