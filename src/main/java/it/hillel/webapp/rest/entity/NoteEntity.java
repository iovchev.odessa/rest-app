package it.hillel.webapp.rest.entity;

import it.hillel.webapp.rest.dao.NoteDao;

import java.util.Map;

public class NoteEntity {
    private Map<Integer, String> notes;
    private int id;
    private String note;

    public NoteEntity() {
    }

    public NoteEntity(int id, String note) {
        this.id = id;
        this.note = note;
    }

    public Map<Integer, String> getNotes() {
        return notes;
    }

    public void setNotes(Map<Integer, String> notes) {
        this.notes = notes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public NoteEntity createNoteEntity(String note) {
        NoteDao noteDao = new NoteDao();
        return noteDao.createNewNote(note);
    }

    public NoteEntity deleteNoteEntity(int id) {
        NoteDao noteDao = new NoteDao();
        return noteDao.deleteNote(id);
    }

    public NoteEntity updateNoteEntity(int id, String note) {
        NoteDao noteDao = new NoteDao();
        return noteDao.updateNote(id, note);
    }

    public NoteEntity getAllNoteEntity() {
        NoteDao noteDao = new NoteDao();
        return noteDao.getAllNote();
    }

    public NoteEntity getNoteEntity(int id) {
        NoteDao noteDao = new NoteDao();
        return noteDao.getNoteById(id);
    }
}
