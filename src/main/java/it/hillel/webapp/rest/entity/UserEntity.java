package it.hillel.webapp.rest.entity;

import it.hillel.webapp.rest.dao.UserDao;

public class UserEntity {
    private int id;
    private String login;
    private String lastName;
    private String firstName;
    private String password;

    public int getId() {
        return id;
    }

    public UserEntity() {
    }

    public UserEntity(int id, String login, String lastName, String firstName, String password) {
        this.id = id;
        this.login = login;
        this.lastName = lastName;
        this.firstName = firstName;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserEntity createUserEntity(String login, String lastName, String firstName, String password) {
        UserDao userDao = new UserDao();
        return userDao.createNewUser(login, lastName, firstName, password);
    }

    public static UserEntity getUserEntityByLogin(String login) {
        UserDao userDao = new UserDao();
        return userDao.getUserByLogin(login);
    }

    public static UserEntity getUserEntityById(int id) {
        UserDao userDao = new UserDao();
        return userDao.getUserById(id);
    }
}
