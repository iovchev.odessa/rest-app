package it.hillel.webapp.rest.validator;

import it.hillel.webapp.rest.exception.ApplicationException;
import it.hillel.webapp.rest.dto.LoginDto;

public class LoginDtoValidator<T> implements Validator<LoginDto> {
    @Override
    public void validate(LoginDto dto) {
        if (dto.getLogin() == null || dto.getLogin().isEmpty()) {
            throw new ApplicationException("Enter login.");
        }

        if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
            throw new ApplicationException("Enter password.");
        }
    }
}
