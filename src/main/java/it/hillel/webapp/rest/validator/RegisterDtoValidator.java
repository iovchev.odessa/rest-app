package it.hillel.webapp.rest.validator;

import it.hillel.webapp.rest.exception.ApplicationException;
import it.hillel.webapp.rest.dto.RegisterDto;

public class RegisterDtoValidator<T> implements Validator<RegisterDto> {
    @Override
    public void validate(RegisterDto dto) {
        if (dto.getLogin() == null || dto.getLogin().isEmpty()) {
            throw new ApplicationException("Enter login.");
        }

        if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
            throw new ApplicationException("Enter password.");
        }

        if (dto.getConfirmPassword() == null || dto.getConfirmPassword().isEmpty()) {
            throw new ApplicationException("Enter confirm password.");
        }

        if (!dto.getPassword().equals(dto.getConfirmPassword())) {
            throw new ApplicationException("Entered passwords are different");
        }
    }
}
